"""
read_folder_and_parse(inputfolder,tag_list,verbose=False)

Parameters:

inputfolder: string, self-explanatory
tag_list: list of dicom tags in python-friendly format, e.g: (0x180081) or (0x18,0x81). First tag should be a UID to sort out different series. Typically SeriesInstanceUID (0x20, 0x0e)
verbose: boolean, optional,  display messages and warnings. Default is false.


Output:

Returns all files in the inputfolder that are valid dicom files and have all the tags specified in tag_list. Output is a list of objects, one for every series found in the folder. Each output object has the following attributes:
.tag_names    : name of the dicom tags specified as input, including the UID tag
.tag_vals     : all the different values found in that one series for all the dicom tags specified as input, discounting the UID tag
.output_array  :  sorted numpy array, number of dimensions = 2 (for x,y) + number of tags in tag_list -1 (the UID tag is not counted as a dimension)


Example:

>>> inputfolder=r'C:\blabla'
>>> tag_list=[(0x20, 0x0e),((0x0020, 0x1041)),(0x08,0x08),(0x180081), (0x19,0x0100c)]  #SeriesUID,SliceLocation,ImageType,EchoTime,bvalue
>>> outputs=read_folder_and_parse(inputfolder,tag_list)
>>> outputs[0].output_array.shape
(130, 130, 25, 1, 1, 7)
>>> outputs[0].tag_names
 ['Slice Location', 'Image Type', 'Echo Time', '[B_value]']
>>> outputs[0].tag_vals[3]
 ['0', '20', '50', '100', '200', '600', '1000']

#######################################################################################

my_dicom_wrapper(output_folder,filename,vol_array,zdim_tag,zdim_vals,refslice,studydesc='')


Parameters:

output_folder: string, self-explanatory
filename: string, generic filename prefix for all output files
vol_array: numpy array, contains pixel data to be output, 3rd coordinate is the slice dimension
zdim_tag: dicom tag, contains the dicom corresponding to the 3rd dimension of vol_array
zdim_vals: list, contains the values corresponding to the z dimension coordinates
refslice: dicom FileDataset, contains a reference dicom slice that will be copied over to create output slices
studydesc: string, description of the output study. Default is blank.


Output:

Creates a dicom series with a new UID from a 3D numpy array, using dicom tag and dicom tag values specified by the user as the slice-wise dimension and slice coordinates. Uses a reference dicom slice provided by the user as a template.

"""

import dicom
import numpy as np
import sys,os,datetime
    
def read_folder_and_parse(inputfolder,tag_list,verbose=False): #first tag must be common identifier for an image stack, e.g. SeriesInstanceUID (0x20, 0x0e)
    class dataslice:                                              
        def __init__(self,file,tag_list):
            self.pixel_array=file.pixel_array
            for tag in tag_list:
                if tag==(0x08,0x08):
                    self.__dict__[tag]=file[tag].value[2]                        #typically R,I,P,M
                else:
                    self.__dict__[tag]=file[tag].value
    class output_object:
        def __init__(self,tag_vals,output_array,tag_names,refslice):
            self.output_array=output_array
            self.tag_vals=tag_vals            
            self.tag_names=tag_names
            self.refslice=refslice
    if verbose==True:
        print('\nMESSAGE: Parsing started') 
        print('\nMESSAGE: Reading files in input folder:\n',inputfolder,'\n')       
    tag_list=[dicom.tag.Tag(tag) for tag in tag_list]                               #convert to dicom.tag.Tag type
    if verbose==True: print('MESSAGE:Commencing parsing, sought after dicom tags are:\n',[tag for tag in tag_list],'\n')
    ds_list=[]
    excluded_files_list=[]
    missing_tag_files_list=[]
    refslices_list=[]
    outputs_list=[]
    UID_list=[]
    if not os.path.exists(inputfolder):                                            
        print('WARNING: Input folder does not exist!\n')
        return
    for dirName,  subdirList,  fileList in os.walk(inputfolder):
        for filename in fileList:
            try:
                file=dicom.read_file(os.path.join(dirName, filename)) 
                if 'PixelData' in file:                                                                           #check if image data is present in the file
                    try:
                        ds=dataslice(file,tag_list)
                        ds_list.append(ds)
                        if len(ds_list)==1:
                            tag_names=[file[tag].name for tag in tag_list]
                            if verbose==True: print('MESSAGE: Tag names are:\n',tag_names,'\n')
                        UID=ds.__dict__[tag_list[0]]
                        if UID not in UID_list:
                            UID_list.append(UID)
                            refslices_list.append(file)
                            outputs_list.append([ds])
                        else:
                            if ds.pixel_array.shape == outputs_list[UID_list.index(UID)][-1].pixel_array.shape:       #check that the dims are the same as previous slice
                                outputs_list[UID_list.index(UID)].append(ds)   #all slices belonging together in image stack i are now in the same list in outputs_list[i]
                            else:
                                raise NameError('WARNING: Found images with same UID but different dimensions')
                    except(KeyError):
                        missing_tag_files_list.append(filename)
            except(dicom.errors.InvalidDicomError):
                excluded_files_list.append(filename)
    if verbose==True:
        if excluded_files_list:
            print('WARNING: the following files are not valid dicoms files and have been excluded:\n',excluded_files_list,end='\n\n')
        if missing_tag_files_list:
            print('WARNING: the following dicom files do not have the sought after dicom tags and have been excluded:\n', missing_tag_files_list,'\n')
        print('MESSAGE: Found',len(ds_list),'valid files \n')
        print('MESSAGE: Found',len(UID_list), 'valid series \n')
    outputs=[]
    for output,UID in zip(outputs_list,UID_list):             #gather all possible tag values for images in same stack, in the order they appear in given tag_list
        tag_vals=[[] for i in range(len(tag_list)-1)]
        for ds in output:
            for i in range(len(tag_list)-1):
                if ds.__dict__[tag_list[i+1]] not in tag_vals[i]:
                    tag_vals[i].append(ds.__dict__[tag_list[i+1]])
        tag_vals=[sorted(tag_vals[i]) for i in range(len(tag_vals))]
        output_shape=ds.pixel_array.shape
        for i in range(len(tag_list)-1):
            output_shape=output_shape+(len(tag_vals[i]),)
        output_array=np.zeros(output_shape)
        for ds in output:
            for tag in tag_list[1:]:                #assign ds.pixel_array to correct indices
                output_array[(slice(None),slice(None))+tuple([tag_vals[tag_list[1:].index(tag)].index(ds.__dict__[tag]) for tag in tag_list[1:]])]=ds.pixel_array
        outputs.append(output_object(tag_vals,output_array,tag_names[1:],refslices_list[UID_list.index(UID)]))     
        if verbose==True:
            print('MESSAGE: Created a',output_array.shape,'array for series UID',UID,'out of a total of',len(output),'files for that series.\n')
            if np.prod(output_array.shape[2:]) != len(output):
                print('WARNING:',np.prod(output_array.shape[2:]),'slices were created out of',len(output),'files. Are you sure the specified dicom tags are discriminating enough?\n')
    return outputs

def my_dicom_wrapper(output_folder,filename,vol_array,zdim_tag,zdim_vals,refslice,studydesc='',verbose=True):  
    if verbose==True:
        print('MESSAGE: Wrapping started') 
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)
    SeriesInstanceUID=dicom.UID.generate_uid()
    timestamp=datetime.datetime.now().strftime("%d_%m_20%y_%H_%M")
    for k in range(vol_array.shape[0]):
        ds=refslice
        ds.PixelData=(np.clip(vol_array[k,:,:],0,vol_array[k,:,:].max()).astype(np.uint16)).tostring()  
        ds[zdim_tag].value=zdim_vals[k]
        ds.InstanceNumber=k
        ds.Rows,ds.Columns=vol_array[k].shape
        ds.SeriesInstanceUID=SeriesInstanceUID
        ds.SeriesDescription=studydesc
        ds.WindowCenter=vol_array.mean()
        ds.WindowWidth=vol_array.max()-vol_array.mean()
        slicename=output_folder+filename+'_'+timestamp+'_slice_'+str(k)+'.dcm'
        ds.save_as(slicename)
    if verbose==True:
        print('MESSAGE: Created',vol_array.shape[0],'slices of dimensions',vol_array.shape[1:],'with UID',SeriesInstanceUID) 
        print('MESSAGE: Wrapping finished') 
        

if __name__ =='__main__':
    
    #example readout
    inputfolder=r'C:\Users\User\Desktop\philips3tdiffupperabdo\diff_test rv with spaces in the name\test_roberto\DICOM'
    tag_list=[(0x20, 0x0e),((0x0020, 0x1041)),(0x08,0x08),(0x180081),(0x19,0x0100c)]  #SeriesUID ,SliceLocation,ImageType, EchoTime  ,bvalue
    # tag_list=[(0x20, 0x0e)]  #SeriesUID
    outputs=read_folder_and_parse(inputfolder,tag_list,verbose=True)

    
    #example post-processing: take first slice, blur images for all b values  
    myarr=np.squeeze(outputs[0].output_array)[:,:,0,:]
    from scipy.ndimage.filters import gaussian_filter
    myarr_blurred = gaussian_filter(myarr, sigma=2)
    output_folder=r'C:\Users\User\Desktop\philips3tdiffupperabdo\diff_test rv with spaces in the name\test_roberto\DICOM\\pp\\'
    filename=r'blurry_pp_image'
    zdim_tag=tag_list[4]      #choice of z dimension, here is b value
    zdim_vals=outputs[0].tag_vals[3]  #corresponding values, seriesUID val has been removed from list, maybe it shouldn't be...
    refslice=outputs[0].refslice   #reference slice to be used
    # my_dicom_wrapper(output_folder,filename,myarr_blurred,zdim_tag,zdim_vals,refslice,studydesc='blu')



#todo: add option for output_array shaping    #if 'xyz': swapaxes(outputarray,0,2)
#todo:  fix parsing with only UID tag













