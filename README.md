read_folder_and_parse(inputfolder,tag_list,verbose=False) 
 

Parameters: 

inputfolder: string, self-explanatory 

tag_list: list of dicom tags in python-friendly format, e.g: (0x180081) or (0x18,0x81). First tag should be a UID to sort out different series. Typically SeriesInstanceUID (0x20, 0x0e)
 
verbose: boolean, optional,  display messages and warnings. Default is false. 
 
 
Output: 
 
Returns all files in the inputfolder that are valid dicom files and have all the tags specified in tag_list. Output is a list of objects, one for every series found in the folder. Each output object has the following attributes: 

.tag_names    : name of the dicom tags specified as input, including the UID tag 

.tag_vals     : all the different values found in that one series for all the dicom tags specified as input, discounting the UID tag 

.output_array  :  sorted numpy array, number of dimensions = 2 (for x,y) + number of tags in tag_list -1 (the UID tag is not counted as a dimension) 
 
 

Example: 
 
> inputfolder=r'C:\blabla' 

> tag_list=[(0x20, 0x0e),((0x0020, 0x1041)),(0x08,0x08),(0x180081), (0x19,0x0100c)]  #SeriesUID,SliceLocation,ImageType,EchoTime,bvalue 

> outputs=read_folder_and_parse(inputfolder,tag_list) 

> outputs[0].output_array.shape 

(130, 130, 25, 1, 1, 7) 

> outputs[0].tag_names 

 ['Slice Location', 'Image Type', 'Echo Time', '[B_value]'] 

> outputs[0].tag_vals[3] 

 ['0', '20', '50', '100', '200', '600', '1000'] 

 
####################################################################################### 
 
my_dicom_wrapper(output_folder,filename,vol_array,zdim_tag,zdim_vals,refslice,studydesc='') 
 
 
Parameters: 
 
output_folder: string, self-explanatory 

filename: string, generic filename prefix for all output files 

vol_array: numpy array, contains pixel data to be output, 3rd coordinate is the slice dimension 

zdim_tag: dicom tag, contains the dicom corresponding to the 3rd dimension of vol_array 

zdim_vals: list, contains the values corresponding to the z dimension coordinates 

refslice: dicom FileDataset, contains a reference dicom slice that will be copied over to create output slices 

studydesc: string, description of the output study. Default is blank. 
 
 
Output: 
 
Creates a dicom series with a new UID from a 3D numpy array, using dicom tag and dicom tag values specified by the user as the slice-wise dimension and slice coordinates. Uses a reference dicom slice provided by the user as a template. 
